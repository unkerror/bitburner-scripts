/** @param {NS} ns **/
export async function main(ns) {
	ns.disableLog("ALL");
	const upgrades = ns.hacknet.getHashUpgrades();
	while (true) {
		ns.hacknet.spendHashes(upgrades[0], "");
		await ns.sleep(1)
	}
}