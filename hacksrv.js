/** @param {NS} ns **/
export async function main(ns) {
    await runHackScript(ns, ns.args[0]);
}

export function autocomplete(data, args) {
	return [...data.servers];
}

/** @param {NS} ns **/
/** @param {string} target **/
async function runHackScript(ns, target) {
	let maxRam = ns.getServerMaxRam(ns.getHostname());
	let usedRam = ns.getServerUsedRam(ns.getHostname());
	let availableRam = maxRam - usedRam;
	ns.print("server max ram: " + maxRam);
	ns.print("server used ram: " + usedRam);
	ns.print("server available ram: " + availableRam);
	let hackRam = ns.getScriptRam("h.script");
	ns.print("hack script ram: " + hackRam);
	let threadCount = (availableRam / hackRam) | 0;
	ns.print("thread count: " + threadCount);
	if (threadCount > 0) {
		ns.spawn("h.script", threadCount, target);
	}
}