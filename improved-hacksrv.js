import * as hackingFormulas from "formulas.js"

export function autocomplete(data, args) {
	return [...data.servers];
}

/** @param {NS} ns **/
export async function main(ns) {
	let formulas = ns.formulas.hacking;
	// let formulas = hackingFormulas;
	var server = ns.args[0];   //Host to hack
	var server2 = ns.getHostname(); //Server to run scripts on
	var i = 0;
	var c = 0;
	var player = ns.getPlayer();
	var fserver = ns.getServer(server);
	//var contstantRam = ns.getScriptRam("improved-hacksrv.js"); //grabbing script RAM values
	var hackscriptRam = ns.getScriptRam("/newserver/hack.js");
	var growscriptRam = ns.getScriptRam("/newserver/grow.js");
	var weakenscriptRam = ns.getScriptRam("/newserver/weaken.js");
	//var maxRam = (ns.getServerMaxRam(server2) - contstantRam); //getting total RAM I can use that doesnt include the OP script
	var maxRam = (ns.getServerMaxRam(server2) - ns.getServerUsedRam(server2)); //getting total RAM I can use that doesnt include the OP script
	var weakenThreads = (2000 - ((ns.getServerMinSecurityLevel(server)) / 0.05));
	var maxGrowThreads = ((maxRam - (weakenscriptRam * 2000) - ns.getServerUsedRam(server2)) / growscriptRam);
	if (maxGrowThreads > 20000) {
		maxGrowThreads = 20000;
	}
	//var maxGrowThreads = ((maxRam / growscriptRam) - (weakenscriptRam * 2000) - ns.getServerUsedRam(server2));
	var cs = ns.getServerSecurityLevel(server);
	var ms = ns.getServerMinSecurityLevel(server);
	var mm = ns.getServerMaxMoney(server);
	var ma = ns.getServerMoneyAvailable(server);

	//Priming the server.  Max money and Min security must be acheived for this to work
	if ((ma < mm) == true) {
		ns.exec('/newserver/weaken.js', server2, 2000, server);
		ns.exec('/newserver/grow.js', server2, maxGrowThreads, server, 0);
		var weakenTime = (formulas.weakenTime(fserver, player));
		await ns.sleep(weakenTime + 1000);
		mm = ns.getServerMaxMoney(server);
		ma = ns.getServerMoneyAvailable(server);
		player = ns.getPlayer();
		fserver = ns.getServer(server);
		cs = ns.getServerSecurityLevel(server);
		ms = ns.getServerMinSecurityLevel(server);
	}

	//If Max Money is true, making sure security level is at its minimum
	if ((cs > ms) == true) {
		ns.exec('/newserver/weaken.js', server2, 2000, server);
		weakenTime = (formulas.weakenTime(fserver, player));
		await ns.sleep(weakenTime + 1000);
		cs = ns.getServerSecurityLevel(server);
		ms = ns.getServerMinSecurityLevel(server);
	}

	//Refreshing server stats now that the security level is at the minmum, and maybe our player stats have changed as priming can take a while
	player = ns.getPlayer();
	fserver = ns.getServer(server);

	var hackPercent = (formulas.hackPercent(fserver, player) * 100);
	var growPercent = (formulas.growPercent(fserver, 1, player, 1));
	weakenTime = (formulas.weakenTime(fserver, player));
	var growTime = (formulas.growTime(fserver, player));
	var hackTime = (formulas.hackTime(fserver, player));

	var growThreads = Math.round(((5 / (growPercent - 1)))); //Getting the amount of threads I need to grow 200%.  I only need 100% but I'm being conservative here
	if (growThreads == 0) {
		growThreads = 1;
	}
	var hackThreads = Math.round((50 / hackPercent));  //Getting the amount of threads I need to hack 50% of the funds
	weakenThreads = Math.round((weakenThreads - (growThreads * 0.004))); //Getting required threads to fully weaken the server

	var totalRamForRun = (hackscriptRam * hackThreads) + (growscriptRam * growThreads) + (weakenscriptRam * weakenThreads) //Calculating how much RAM is used for a single run
	//ns.tprint("Total ram for run: " + totalRamForRun);
	var sleepTime = (weakenTime / (maxRam / totalRamForRun)) //finding how many runs this server can handle and setting the time between run execution

	var shiftCount = maxRam / totalRamForRun;
	var offset = sleepTime / 2
	var gOffset = offset / 4
	var hOffset = offset / 2


	while (true) {
		var gsleep = ((weakenTime - growTime - gOffset)); //Getting the time to have the Growth execution sleep, then shaving some off to beat the weaken execution
		var hsleep = ((weakenTime - hackTime - hOffset)); //Getting time for hack, shaving off more to make sure it beats both weaken and growth
		var usedRam = ns.getServerUsedRam(server2);

		if ((totalRamForRun >= (maxRam - usedRam)) == false) //making sure I have enough RAM to do a full run
		{
			ns.exec('/newserver/weaken.js', server2, weakenThreads, server, i);
			ns.exec('/newserver/grow.js', server2, growThreads, server, gsleep, i);
			ns.exec('/newserver/hack.js', server2, hackThreads, server, hsleep, i);

			if (c < shiftCount) {
				await ns.sleep(sleepTime)
				c++
			}
			else {
				await ns.sleep(sleepTime + offset);
				c = 0;
			}
			i++
		}
		else {
			await ns.sleep(1000)
		}
	}
}