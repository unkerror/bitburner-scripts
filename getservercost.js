/** @param {NS} ns **/
export async function main(ns) {
    if (ns.args.length == 3 && ns.args[0] == "buy") {
        ns.purchaseServer(ns.args[1], ns.args[2]);
    } else if (ns.args.length == 2 && ns.args[0] == "sell") {
        ns.deleteServer(ns.args[1]);
    }
    for (let i = 1; i <= 20; i++) {
        ns.tprint(i + " -- " + Math.pow(2, i) + " -- " + ns.getPurchasedServerCost(Math.pow(2, i)));
    }
}

export function autocomplete(data, args) {
    return ["buy", "sell"]
}