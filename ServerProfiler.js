let allHosts;
let allServers;

class Server {
	maxRam;
	availableRam;
	maxMoney;
	availableMoney;
	securityLevel;
	minSecurityLevel;
	growthRate;
	requiredHackingLevel;
	portsRequired;
	hasRootAccess;
	host;
	path;
}

/** @param {NS} ns **/
export async function main(ns) {
	allHosts = ns.scan();
	allServers = [];
	let myHosts = [].concat(allHosts);
	for (let i = 0; i < myHosts.length; i++) {
		await scanServer(ns, myHosts[i], ["home"]);
	}
	if (allServers.length > 0) {
		await ns.write("allServers.txt", JSON.stringify(allServers), "w");
		let maxMoneyServer = allServers[0];
		let maxGrowthServer = allServers[0];
		let maxMoneyAvailableServer = allServers[0];
		let myhackingSkill = ns.getHackingLevel();
		if (allServers[0].host == "n00dles") {
			maxGrowthServer = allServers[1];
		}
		for (let i = 0; i < allServers.length; i++) {
			let server = allServers[i];
			if (server.maxMoney > maxMoneyServer.maxMoney && server.hasRootAccess && ns.getServerRequiredHackingLevel(allServers[i].host) <= myhackingSkill) {
				maxMoneyServer = server;
			}
			if (server.availableMoney > maxMoneyAvailableServer.availableMoney && server.hasRootAccess && ns.getServerRequiredHackingLevel(allServers[i].host) <= myhackingSkill) {
				maxMoneyAvailableServer = server;
			}
			if (server.growthRate > maxGrowthServer.growthRate && server.hasRootAccess && server.host != "n00dles" && ns.getServerRequiredHackingLevel(allServers[i].host) <= myhackingSkill) {
				maxGrowthServer = server;
			}
			if (server.host == ns.args[0]) {
				ns.tprint("Server: \t\t\t" + server.host);
				ns.tprint("Path: \n" + server.path.join("; connect "));
				ns.tprint("Max Money: \t\t\t" + server.maxMoney);
				ns.tprint("Money: \t\t\t" + server.availableMoney);
				ns.tprint("Max RAM: \t\t\t" + server.maxRam);
				ns.tprint("Available RAM: \t\t" + server.availableRam);
				ns.tprint("Security Level: \t\t" + server.securityLevel);
				ns.tprint("Min Security Level: \t\t" + server.minSecurityLevel);
				ns.tprint("Growth Rate: \t\t" + server.growthRate);
				ns.tprint("Required Hacking Level: \t" + server.requiredHackingLevel);
				ns.tprint("Ports Required For Hack: \t" + server.portsRequired);
				ns.tprint("Has Root Access: \t\t" + server.hasRootAccess);
			}
		}
		ns.tprint("Max Available Money Server: " + maxMoneyAvailableServer.host);
		ns.tprint("Max Money Server: " + maxMoneyServer.host);
		ns.tprint("Max Growth Server: " + maxGrowthServer.host);
	}
}

export function autocomplete(data, args) {
	return [...data.servers];
}

/** @param {NS} ns **/
/** @param {string} host **/
async function scanServer(ns, host, path) {
	if (host == ns.getHostname()) {
		return;
	}

	let isRooted = ns.hasRootAccess(host);

	if (host != "home") {
		let server = new Server();
		server.host = host;
		server.path = [...path, host];
		server.maxMoney = ns.getServerMaxMoney(host);
		server.availableMoney = ns.getServerMoneyAvailable(host);
		server.maxRam = ns.getServerMaxRam(host);
		server.availableRam = server.maxRam - ns.getServerUsedRam(host);
		server.growthRate = ns.getServerGrowth(host);
		server.minSecurityLevel = ns.getServerMinSecurityLevel(host);
		server.securityLevel = ns.getServerSecurityLevel(host);
		server.requiredHackingLevel = ns.getServerRequiredHackingLevel(host);
		server.portsRequired = ns.getServerNumPortsRequired(host);
		server.hasRootAccess = isRooted;
		allServers.push(server);
	}

	let thisHosts = ns.scan(host);
	for (let i = 0; i < thisHosts.length; i++) {
		if (!allHosts.includes(thisHosts[i], 0)) {
			allHosts.push(thisHosts[i]);
			await scanServer(ns, thisHosts[i], [...path, host]);
		}
	}
}