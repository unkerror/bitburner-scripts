/** @param {NS} ns **/
export async function main(ns) {
    let filesToDownload = [
        "autocontracts.js",
        "auto-hashes.js",
        "autoroot.js",
        "autoroot2.js",
        "autostanek.js",
        "backdoor.js",
        "buy-exploits.js",
        "calcsrv.js",
        "charge-stanek.js",
        "check-karma.js",
        "crimes/commitCrime.js",
        "crimes/getCrimesData.js",
        "crimes/getCrimesData2.js",
        "crimes/karmafarm.js",
        "copysrv.js",
        "copytoallsrv.js",
        "formulas.js",
        "g.script",
        "getservercost.js",
        "growsrv.js",
        "h.script",
        "hackall.js",
        "hacknet.js",
        "hacksrv.js",
        "hacksrv1.js",
        "hacksrv1-improved.js",
        "improved-hacksrv.js",
        "newserver/grow.js",
        "newserver/hack.js",
        "newserver/weaken.js",
        "sellhashes.js",
        "ServerProfiler.js",
        "stock-trader.js",
        "tix-stock-trader.js",
        "w.script",
        "weaksrv.js"];
    for (let i=0; i<filesToDownload.length; i++) {
        let scriptFile = filesToDownload[i];
        await ns.wget("https://bitbucket.org/unkerror/bitburner-scripts/raw/main/" + scriptFile, scriptFile, "home");
    }
}