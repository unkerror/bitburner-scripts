let allHosts;
let allServers;

class Server {
	maxRam;
	availableRam;
	maxMoney;
	availableMoney;
	securityLevel;
	minSecurityLevel;
	growthRate;
	requiredHackingLevel;
	portsRequired;
	hasRootAccess;
	host;
	path;
}

/** @param {NS} ns **/
export async function main(ns) {
	allHosts = ns.scan();
	allServers = [];
	let myHosts = [].concat(allHosts);
	for (let i = 0; i < myHosts.length; i++) {
		await scanServer(ns, myHosts[i], ["home"]);
	}
	if (allServers.length > 0) {
		for (let i = 0; i < allServers.length; i++) {
			let server = allServers[i];
			if (server.host == ns.args[0] && server.hasRootAccess) {
				for (let j = 0; j<server.path.length; j++) {
					ns.singularity.connect(server.path[j]);
				}
				// doesn't actually return true when successful
				await ns.singularity.installBackdoor();
				ns.tprint("Installed backdoor on " + server.host);
				ns.singularity.connect("home");
			}
		}
	}
}

export function autocomplete(data, args) {
	return [...data.servers];
}

/** @param {NS} ns **/
/** @param {string} host **/
async function scanServer(ns, host, path) {
	if (host == ns.getHostname()) {
		return;
	}

	let isRooted = ns.hasRootAccess(host);

	if (host != "home") {
		let server = new Server();
		server.host = host;
		server.path = [...path, host];
		server.maxMoney = ns.getServerMaxMoney(host);
		server.availableMoney = ns.getServerMoneyAvailable(host);
		server.maxRam = ns.getServerMaxRam(host);
		server.availableRam = server.maxRam - ns.getServerUsedRam(host);
		server.growthRate = ns.getServerGrowth(host);
		server.minSecurityLevel = ns.getServerMinSecurityLevel(host);
		server.securityLevel = ns.getServerSecurityLevel(host);
		server.requiredHackingLevel = ns.getServerRequiredHackingLevel(host);
		server.portsRequired = ns.getServerNumPortsRequired(host);
		server.hasRootAccess = isRooted;
		allServers.push(server);
	}

	let thisHosts = ns.scan(host);
	for (let i = 0; i < thisHosts.length; i++) {
		if (!allHosts.includes(thisHosts[i], 0)) {
			allHosts.push(thisHosts[i]);
			await scanServer(ns, thisHosts[i], [...path, host]);
		}
	}
}