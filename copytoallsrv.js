/** @param {NS} ns **/
export async function main(ns) {
	let jsFiles = ns.ls(ns.getHostname(), ".js");
	let purchasedServers = ns.getPurchasedServers();
	for (let i = 0; i < purchasedServers.length; i++) {
		await ns.scp(jsFiles, purchasedServers[i], ns.getHostname());
	}
}