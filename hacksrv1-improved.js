/** @param {NS} ns **/
export async function main(ns) {
    //ns.disableLog("ALL");
	let hostname = ns.args[0];
	let weakenThreads = ns.args[1];
	let growThreads = ns.args[2];
	let hackThreads = ns.args[3];
	let targetSecurityLevel = ns.args[4];
	let targetMoney = ns.args[5];
	// let weakenOpts = {
	// 	stock: false,
	// 	threads: weakenThreads
	// };
	let growOpts = {
		stock: false,
		threads: growThreads
	};
	let hackOpts = {
		stock: false,
		threads: hackThreads
	};
	while (true) {
		if (ns.getServerMoneyAvailable(hostname) < targetMoney && ns.getServerSecurityLevel(hostname) <= targetSecurityLevel) {
			await ns.grow(hostname, growOpts);
		} else if (ns.getServerSecurityLevel(hostname) <= targetSecurityLevel) {
			let stolenMoney = 0;
			do {
				stolenMoney = await ns.hack(hostname, hackOpts);
			} while(stolenMoney == 0);
		} else {
			await ns.weaken(hostname);
		}
	}
}