export async function main(ns) {
	ns.disableLog("ALL");
	const crimeToCommit = 'homicide';
	while (ns.heart.break() > -54000) { // loop while karma greater than -54000
		while (ns.singularity.isBusy()) {
			await ns.sleep(100);
		}
		ns.singularity.commitCrime(crimeToCommit);
	}
}