/** @param {NS} ns **/
export async function main(ns) {
    //ns.disableLog("ALL");
	let hostname = ns.args[0];
	let targetSecurityLevel = ns.args[1];
	let targetMoney = ns.args[2];
	while (true) {
		if (ns.getServerSecurityLevel(hostname) > targetSecurityLevel) {
			await ns.weaken(hostname);
		} else if (ns.getServerMoneyAvailable(hostname) < targetMoney) {
			await ns.grow(hostname);
		} else {
			await ns.hack(hostname);
		}
	}
}