/** @param {NS} ns **/
export async function main(ns) {
	const defaultMaxThreads = Infinity;
	let maxThreads = ns.args.length == 1 ? ns.args[0] | 0 : defaultMaxThreads;
	let runOnServer = ns.getHostname();
	let thisScriptRam = ns.getScriptRam("autostanek.js", runOnServer);
	let availableRam = ns.getServerMaxRam(runOnServer) - ns.getServerUsedRam(runOnServer) + thisScriptRam;
	let scriptRam = ns.getScriptRam("charge-stanek.js", runOnServer);
	let threadCount = (availableRam / scriptRam) | 0;
	threadCount = threadCount > maxThreads ? maxThreads : threadCount;
	var activeFragments = ns.stanek.activeFragments();
	await ns.write("stanek-fragments.txt", JSON.stringify(activeFragments), "w");
	if (threadCount > 0) {
		ns.spawn("charge-stanek.js", threadCount);
	}
}