/** @param {NS} ns **/
export async function main(ns) {
    await runGrowScript(ns, ns.args[0]);
}

export function autocomplete(data, args) {
	return [...data.servers];
}

/** @param {NS} ns **/
/** @param {string} target **/
async function runGrowScript(ns, target) {
	let maxRam = ns.getServerMaxRam(ns.getHostname());
	ns.print("server max ram: " + maxRam);
	let hackRam = ns.getScriptRam("g.script");
	ns.print("hack script ram: " + hackRam);
	let threadCount = (maxRam / hackRam) | 0;
	ns.print("thread count: " + threadCount);
	if (threadCount > 0) {
		ns.spawn("g.script", threadCount, target);
	}
}