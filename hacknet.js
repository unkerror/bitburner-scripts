/** @param {NS} ns **/
export async function main(ns) {
	//let baseHacknetNodeProduction = 1.5;
    ns.disableLog("getServerMoneyAvailable");
	// level upgrade production increase
	// ram upgrade production increase .0525 * level + ram bonus + core bonus
	// (.035 + (.35 * cores)/20) * level
	// ((x * currentRamLevel) + (.35 * cores)/20) * level
	// x = ram multiplier, y = core multiplier
	// (x * 1) + (y * 1) = .0525
	// (x * 1) + (y * 2) = .0525
	// .0525 - level 1, ram 1, core 1
	// .105 - level 2, ram 1, core 1
	// .1575 - level 3, ram 1, core 1
	// .210 - level 4, ram 1, core 1
	// .2625 - level 5, ram 1, core 1
	// .315 - level 6, ram 1, core 1
	// .3675 - level 7, ram 1, core 1
	// .6635 - level 6, ram 2, core 1
	// core upgrade production increase (.25 + (.0525 * ram)/2) * level
	// 1.00 - level 4, ram 1, core 1
	// 1.25 - level 5, ram 1, core 1
	// 1.5 - level 6, ram 1, core 1
	// 1.553 - level 6, ram 2, core 1
	// 1.75 - level 7, ram 1, core 1
	// 1.811 - level 7, ram 2, core 1
	// 1.811 - level 7, ram 2, core 2
	// 1.811 - level 7, ram 2, core 3
	let myMoney = ns.getServerMoneyAvailable("home");
	let hacknet = ns.hacknet;
	let n = 1; // number of times to upgrade
	let reserveMoney = 1000;
	if (hacknet.numNodes() === 0) {
		hacknet.purchaseNode();
	}

	while (hacknet.numNodes() > 0) {
		while ((myMoney = ns.getServerMoneyAvailable("home")) >= reserveMoney) {
			let lastNodeLevelCost = Infinity;
			let lastNodeRamCost = Infinity;
			let lastNodeCoreCost = Infinity;

			for (let i = 0; i < hacknet.numNodes(); i++) {
				lastNodeLevelCost = hacknet.getLevelUpgradeCost(i, n);
				while (lastNodeLevelCost < Infinity && hacknet.upgradeLevel(i, n)) {
					//ns.print("Upgraded node " + i + " level");
					await ns.sleep(1);
				}
				lastNodeRamCost = hacknet.getRamUpgradeCost(i, n);
				while (lastNodeRamCost < Infinity && hacknet.upgradeRam(i, n)) {
					//ns.print("Upgraded node " + i + " ram");
					await ns.sleep(1);
				}
				lastNodeCoreCost = hacknet.getCoreUpgradeCost(i, n);
				while (lastNodeCoreCost < Infinity && hacknet.upgradeCore(i, n)) {
					//ns.print("Upgraded node " + i + " cores");
					await ns.sleep(1);
				}
			}

			if (lastNodeLevelCost === Infinity && lastNodeRamCost === Infinity && lastNodeCoreCost === Infinity) {
				if (hacknet.numNodes() < 23) {
					// purchase up to 23 nodes
					hacknet.purchaseNode();
				}
			} else if (lastNodeLevelCost > hacknet.getPurchaseNodeCost() && lastNodeRamCost > hacknet.getPurchaseNodeCost() && lastNodeCoreCost > hacknet.getPurchaseNodeCost()) {
				// cheaper to buy a node than to upgrade
				hacknet.purchaseNode();
			}
			await ns.sleep(1);
		}
		await ns.sleep(1);
	}
}