/** @param {NS} ns **/
export async function main(ns) {
    ns.disableLog("getServerSecurityLevel");
	while (true) {
		let scriptOptionsJson = await ns.read("autoroot2-config.txt");
		let scriptOptions = JSON.parse(scriptOptionsJson);
		let growOpts = {
			stock: false,
			threads: scriptOptions.growThreads
		};
		let hackOpts = {
			stock: false,
			threads: scriptOptions.hackThreads
		};
		if (ns.getServerMoneyAvailable(scriptOptions.target) < scriptOptions.targetMoney && ns.getServerSecurityLevel(scriptOptions.target) <= scriptOptions.targetSecurityLevel) {
			await ns.grow(scriptOptions.target, growOpts);
		} else if (ns.getServerSecurityLevel(scriptOptions.target) <= scriptOptions.targetSecurityLevel) {
			let stolenMoney = 0;
			do {
				stolenMoney = await ns.hack(scriptOptions.target, hackOpts);
			} while(stolenMoney == 0);
		} else {
			await ns.weaken(scriptOptions.target);
		}
	}
}