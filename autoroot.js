let allHosts;
let allHackedServers;

class Server {
	maxRam;
	availableMoney;
	host;
}

export function autocomplete(data, args) {
	return [...data.scripts];
}

/** @param {NS} ns **/
export async function main(ns) {
	ns.disableLog("ALL");
	let hackScript = ns.args[0] || "autoroot2.js";
	let blacklistedServers = [
		"megacorp",
		"ecorp",
		"kuai-gong",
		"omnitek",
		"icarus",
		"fulcrumtech",
		"galactic-cyber",
		"nwo",
		"univ-energy",
		"microdyne",
		"zb-institute",
		"global-pharm",
		"unitalife",
		"4sigma",
		"applied-energetics",
		"taiyang-digital",
		"stormtech",
		"helios",
		"nova-med",
		"zeus-med",
		"solaris",
		"netlink",
		"zb-def",
		"defcomm",
		"clarkinc",
		"syscore"
	];
	while (true) {
		allHosts = ns.scan();
		allHackedServers = [];
		let serversWithContracts = [];
		let myHosts = [].concat(allHosts);
		for (let i = 0; i < myHosts.length; i++) {
			await autoRoot(ns, myHosts[i]);
		}
		if (allHackedServers.length > 0) {
			let maxMoneyServer = allHackedServers[0];
			for (let i = 0; i < allHackedServers.length; i++) {
				if (allHackedServers[i].availableMoney > maxMoneyServer.availableMoney && !blacklistedServers.includes(allHackedServers[i].host)) {
					maxMoneyServer = allHackedServers[i];
				}
			}
			for (let i = 0; i < allHackedServers.length; i++) {
				//ns.tprint("Hacked server: " + allHackedServers[i].host);
				await copyScripts(ns, allHackedServers[i].host);
				if (/*allHackedServers[i].host != "CSEC" &&*/ allHackedServers[i].host != "omnitek") {
					await runHackScriptOnServer(ns, allHackedServers[i].host, maxMoneyServer.host, hackScript);
				}
				// 11.65 GB without this
				// find cct files
				// let codingContracts = ns.ls(allHackedServers[i].host, ".cct");
				// if (codingContracts.length > 0) {
				// 	serversWithContracts.push(allHackedServers[i].host);
				// 	//ns.tprint(allHackedServers[i].host + " has hacking contracts");
				// }
			}
			// await ns.write("serversWithContracts.txt", JSON.stringify(serversWithContracts), "w");
			ns.write("hackedServers.txt", JSON.stringify(allHackedServers), "w");
		}
		await ns.sleep(10000);
	}
}

/** @param {NS} ns **/
/** @param {string} host **/
/** @param {string} host **/
async function runHackScriptOnServer(ns, host, target, hackScript) {
	let targetSecurityLevel = ns.getServerMinSecurityLevel(target);
	let targetMoney = (ns.getServerMaxMoney(target) / 2) | 0;
	if (targetMoney < 0) {
		targetMoney = Math.pow(2,32) - 1;
	}
	let availableRam = ns.getServerMaxRam(host);
	let scriptRam = ns.getScriptRam(hackScript, host);
	let threadCount = (availableRam / scriptRam) | 0;
	let growThreads = ns.growthAnalyze(target, ns.getServerMaxMoney(target), 1) | 0 + 1;
	if (growThreads > threadCount) {
		growThreads = threadCount;
	}
	let growSecurityGain = 0.004 * growThreads;
	let hackThreads = (1 / ns.hackAnalyze(target)) | 0 + 1;
	if (hackThreads > threadCount) {
		hackThreads = threadCount;
	}
	let hackSecurityGain = ns.hackAnalyzeSecurity(hackThreads);
	let weakThreads = (growSecurityGain + hackSecurityGain) / 0.05 | 0 + 1;
	if (threadCount > weakThreads && threadCount > hackThreads && threadCount > growThreads) {
		// reduce thread count to match what's needed
		if (weakThreads > growThreads && weakThreads > hackThreads) {
			threadCount = weakThreads;
		} else if (growThreads > weakThreads && growThreads > hackThreads) {
			threadCount = growThreads;
		} else {
			threadCount = hackThreads;
		}
		weakThreads = threadCount; // use all available threads for weakening
	}
	if (weakThreads > threadCount) {
		weakThreads = threadCount;
	}
	if (growThreads > threadCount) {
		growThreads = threadCount;
	}
	if (hackThreads > threadCount) {
		hackThreads = threadCount;
	}
	targetSecurityLevel = targetSecurityLevel + hackSecurityGain;
	let scriptOptions = {
		hackThreads: hackThreads,
		weakThreads: weakThreads,
		growThreads: growThreads,
		target: target,
		targetSecurityLevel: targetSecurityLevel,
		targetMoney: targetMoney
	};
	await ns.write("autoroot2-config.txt", JSON.stringify(scriptOptions), "w");
	await ns.scp("autoroot2-config.txt", host, ns.getHostname());
	if (!ns.scriptRunning(hackScript, host)) {
		await ns.killall(host);
		let maxRam = await ns.getServerMaxRam(host);
		let hackRam = await ns.getScriptRam(hackScript);
		let threadCount = (maxRam / hackRam) | 0;
		if (threadCount > 0) {
			let pid = ns.exec(hackScript, host, threadCount, host);
			if (pid > 0) {
				ns.print("ran hack script on " + host + " with pid " + pid);
			}
		} else {
			//ns.print("skipping hack script on " + host);
		}
	}
}

/** @param {NS} ns **/
/** @param {string} host **/
async function autoRoot(ns, host) {
	if (host == ns.getHostname()) {
		return;
	}
	//ns.print("running autoRoot on " + host);
	let myHackingSkill = await ns.getHackingLevel();
	let myExploitsCount = await getNumExploits(ns);
	//ns.print("hosts " + allHosts.join(", "));
	let isRooted = ns.hasRootAccess(host);
	if (!isRooted
		&& ns.getServerRequiredHackingLevel(host) <= myHackingSkill
		&& ns.getServerNumPortsRequired(host) <= myExploitsCount) {
		ns.print("hacking node " + host);
		await root(ns, host);
		isRooted = true;
	} else if (!isRooted) {
		//ns.print("hacking skill: " + myHackingSkill + " hacking skill required: " + ns.getServerRequiredHackingLevel(host));
		//ns.print("my exploits: " + myExploitsCount + " exploits required: " + ns.getServerNumPortsRequired(host));
		//ns.print("skipping node " + host);
		return;
	}

    if (isRooted && host != "home" && !ns.getPurchasedServers().includes(host)) {
		let server = new Server();
		server.availableMoney = ns.getServerMoneyAvailable(host);
		server.maxRam = ns.getServerMaxRam(host);
		server.host = host;
		allHackedServers.push(server);
	}

	let thisHosts = ns.scan(host);
	for (let i = 0; i < thisHosts.length; i++) {
		if (!allHosts.includes(thisHosts[i], 0)) {
			allHosts.push(thisHosts[i]);
			await autoRoot(ns, thisHosts[i]);
		}
	}
}

/** @param {NS} ns **/
async function getNumExploits(ns) {
	let numExploits = 0;
	const exploits = ["BruteSSH.exe", "FTPCrack.exe", "relaySMTP.exe", "HTTPWorm.exe", "SQLInject.exe"];
	exploits.forEach((exploit, idx, arr) => {
		if (ns.fileExists(exploit)) {
			numExploits++;
		}
	});
	return numExploits;
}

/** @param {NS} ns **/
/** @param {string} host **/
async function root(ns, host) {
	if (ns.fileExists("BruteSSH.exe", "home")) {
		await ns.brutessh(host);
	}
	if (ns.fileExists("FTPCrack.exe", "home")) {
		await ns.ftpcrack(host);
	}
	if (ns.fileExists("relaySMTP.exe", "home")) {
		await ns.relaysmtp(host);
	}
	if (ns.fileExists("HTTPWorm.exe", "home")) {
		await ns.httpworm(host);
	}
	if (ns.fileExists("SQLInject.exe", "home")) {
		await ns.sqlinject(host);
	}

	await ns.nuke(host);
}

/** @param {NS} ns **/
/** @param {string} host **/
async function copyScripts(ns, host) {
	let nsFiles = ns.ls("home", ".ns");
	let scriptFiles = ns.ls("home", ".script");
	let jsFiles = ns.ls("home", ".js");
	await ns.scp(nsFiles.concat(scriptFiles).concat(jsFiles), host, "home");
}