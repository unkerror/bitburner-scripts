/** @param {NS} ns **/
export async function main(ns) {
    let jsFiles = ns.ls(ns.getHostname(), ".js");
	let scriptFiles = ns.ls(ns.getHostname(), ".script");
	ns.scp(jsFiles.concat(scriptFiles), ns.args[0], ns.getHostname());
}

export function autocomplete(data, args) {
	return [...data.servers];
}