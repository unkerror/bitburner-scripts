/** @param {NS} ns */
export async function main(ns) {
	ns.getHostname();
	ns.getScriptRam("bigscript.js");
	ns.getServerMaxRam("n00dles");
	ns.getServerUsedRam("n00dles");
	ns.getServerMaxMoney("n00dles");
	ns.getServerMoneyAvailable("n00dles");
	ns.getServerSecurityLevel("n00dles");
	ns.getServerMinSecurityLevel("n00dles");
	let player = ns.getPlayer();
	ns.getServer("n00dles");
	ns.formulas.hacking.hackPercent("n00dles", player);
	ns.formulas.hacking.growPercent("n00dles", 1, player, 1);
	ns.formulas.hacking.weakenTime("n00dles", player);
	ns.formulas.hacking.growTime("n00dles", player);
	ns.formulas.hacking.hackTime("n00dles", player);
	ns.getServerUsedRam("home");
	ns.singularity.commitCrime("Shoplift");
	ns.hacknet.getCacheUpgradeCost(0, 1);
	ns.hacknet.getCoreUpgradeCost(0, 1);
	if (!ns.hasRootAccess("n00dles")) {
		ns.singularity.connect("n00dles");
		ns.singularity.purchaseTor();
		ns.singularity.purchaseProgram("BruteSSH.exe");
		ns.brutessh("n00dles");
		ns.relaysmtp("n00dles");
		ns.httpworm("n00dles");
		ns.sqlinject("n00dles");
		ns.ftpcrack("n00dles");
		ns.nuke("n00dles");
		await ns.singularity.installBackdoor();
	}
	ns.getBitNodeMultipliers();
	ns.getFavorToDonate();
	ns.getGrowTime("n00dles");
	ns.getHackTime("n00dles");
	ns.getWeakenTime("n00dles");
	ns.getHacknetMultipliers();
	ns.singularity.getOwnedSourceFiles();
	await ns.hack("n00dles");
	ns.exec('/check-karma.js', "home");
}