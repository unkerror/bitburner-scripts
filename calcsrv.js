/** @param {NS} ns **/
export async function main(ns) {
	let hostname = ns.args[0];
	const defaultMaxThreads = Infinity;
	let maxThreads = ns.args.length == 2 ? ns.args[1] | 0 : defaultMaxThreads;
	let runOnServer = ns.getHostname();
	let targetSecurityLevel = ns.getServerMinSecurityLevel(hostname);
	let targetMoney = (ns.getServerMaxMoney(hostname) / 2) | 0;
	if (targetMoney < 0) {
		targetMoney = Math.pow(2,32) - 1;
	}
	let thisScriptRam = ns.getScriptRam("calcsrv.js", runOnServer);
	let availableRam = ns.getServerMaxRam(runOnServer) - ns.getServerUsedRam(runOnServer) + thisScriptRam;
	let scriptRam = ns.getScriptRam("hacksrv1-improved.js", runOnServer);
	let threadCount = (availableRam / scriptRam) | 0;
	threadCount = threadCount > maxThreads ? maxThreads : threadCount;
	let growThreads = ns.growthAnalyze(hostname, ns.getServerMaxMoney(hostname), 1) | 0 + 1;
	ns.tprint("grow threads: " + growThreads);
	if (growThreads > threadCount) {
		growThreads = threadCount;
	}
	let growSecurityGain = 0.004 * growThreads;
	let hackThreads = (1 / ns.hackAnalyze(hostname)) | 0 + 1;
	ns.tprint("hack threads: " + hackThreads);
	if (hackThreads > threadCount) {
		hackThreads = threadCount;
	}
	let hackSecurityGain = ns.hackAnalyzeSecurity(hackThreads);
	let weakThreads = (growSecurityGain + hackSecurityGain) / 0.05 | 0 + 1;
	ns.tprint("weak threads: " + weakThreads);
	if (threadCount > weakThreads && threadCount > hackThreads && threadCount > growThreads) {
		// reduce thread count to match what's needed
		if (weakThreads > growThreads && weakThreads > hackThreads) {
			threadCount = weakThreads;
		} else if (growThreads > weakThreads && growThreads > hackThreads) {
			threadCount = growThreads;
		} else {
			threadCount = hackThreads;
		}
		weakThreads = threadCount; // use all available threads for weakening
	}
	if (weakThreads > threadCount) {
		weakThreads = threadCount;
	}
	if (growThreads > threadCount) {
		growThreads = threadCount;
	}
	if (hackThreads > threadCount) {
		hackThreads = threadCount;
	}
	targetSecurityLevel = targetSecurityLevel + hackSecurityGain;
	targetMoney = (ns.hackAnalyze(hostname) * hackThreads) * targetMoney;
	if (ns.args.length < 3) {
		ns.spawn("hacksrv1-improved.js", threadCount, hostname, weakThreads, growThreads, hackThreads, targetSecurityLevel, targetMoney);
	}
}

export function autocomplete(data, args) {
	return [...data.servers];
}