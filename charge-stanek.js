/** @param {NS} ns **/
export async function main(ns) {
	let boosterFragments = [
		101
	];
	while (true) {
		let scriptOptionsJson = await ns.read("stanek-fragments.txt");
		let scriptOptions = JSON.parse(scriptOptionsJson);
		for (let i = 0; i < scriptOptions.length; i++) {
			let fragment = ns.stanek.getFragment(scriptOptions[i].x, scriptOptions[i].y);
			let boosterFragmentIndex = boosterFragments.indexOf(fragment.id);
			if (boosterFragmentIndex == -1) {
				// ns.tprint("booster fragment index: " + boosterFragmentIndex);
				// ns.tprint("fragment id: " + fragment.id);
				await ns.stanek.chargeFragment(scriptOptions[i].x, scriptOptions[i].y);
			}/* else {
				ns.tprint("booster fragment index: " + boosterFragmentIndex);
			}*/
		}
		await ns.sleep(1);
	}
}