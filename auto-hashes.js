let allHosts;
let allServers;
const reduceSecurity = "Reduce Minimum Security";
//const reduceSecurity = "Exchange for Bladeburner Rank";
//const reduceSecurity = "Exchange for Bladeburner SP";
//const reduceSecurity = "Generate Coding Contract";
class Server {
	maxMoney;
	availableMoney;
	securityLevel;
	minSecurityLevel;
	growthRate;
	requiredHackingLevel;
	portsRequired;
	hasRootAccess;
	host;
	path;
}

/** @param {NS} ns **/
export async function main(ns) {
	ns.disableLog("sleep");
	ns.disableLog("getHackingLevel");
	ns.disableLog("scan");
	ns.disableLog("getServerMinSecurityLevel");
	ns.disableLog("hasRootAccess");
	ns.disableLog("getServerMaxMoney");
	ns.disableLog("getServerGrowth");
	ns.disableLog("getServerRequiredHackingLevel");
	while (true) {
		allHosts = ns.scan();
		allServers = [];
		let myHosts = [].concat(allHosts);
		for (let i = 0; i < myHosts.length; i++) {
			await scanServer(ns, myHosts[i], ["home"]);
		}
		if (allServers.length > 0) {
			let maxMoneyServer = allServers[0];
			let maxGrowthServer = allServers[0];
			let myhackingSkill = ns.getHackingLevel();
			if (allServers[0].host == "n00dles") {
				maxGrowthServer = allServers[1];
			}
			for (let i = 0; i < allServers.length; i++) {
				let server = allServers[i];
				if (server.maxMoney > maxMoneyServer.maxMoney && server.hasRootAccess && ns.getServerRequiredHackingLevel(allServers[i].host) <= myhackingSkill) {
					maxMoneyServer = server;
				}
				if (server.growthRate > maxGrowthServer.growthRate && server.hasRootAccess && server.host != "n00dles" && ns.getServerRequiredHackingLevel(allServers[i].host) <= myhackingSkill && ns.getServerMinSecurityLevel(allServers[i].host) > 1.0) {
					maxGrowthServer = server;
				}
			}
			if (ns.hacknet.numHashes() > ns.hacknet.hashCost(reduceSecurity) && maxGrowthServer.minSecurityLevel > 1.0) {
				ns.print("Reducing min security of " + maxGrowthServer.host);
				ns.hacknet.spendHashes(reduceSecurity, maxGrowthServer.host);
			}
		}
		await ns.sleep(1);
	}
}

export function autocomplete(data, args) {
	return [...data.servers];
}

/** @param {NS} ns **/
/** @param {string} host **/
async function scanServer(ns, host, path) {
	if (host == ns.getHostname()) {
		return;
	}

	let isRooted = ns.hasRootAccess(host);

	if (host != "home") {
		let server = new Server();
		server.host = host;
		server.path = [...path, host];
		server.maxMoney = ns.getServerMaxMoney(host);
		server.growthRate = ns.getServerGrowth(host);
		server.minSecurityLevel = ns.getServerMinSecurityLevel(host);
		server.requiredHackingLevel = ns.getServerRequiredHackingLevel(host);
		server.hasRootAccess = isRooted;
		allServers.push(server);
	}

	let thisHosts = ns.scan(host);
	for (let i = 0; i < thisHosts.length; i++) {
		if (!allHosts.includes(thisHosts[i], 0)) {
			allHosts.push(thisHosts[i]);
			await scanServer(ns, thisHosts[i], [...path, host]);
		}
	}
}